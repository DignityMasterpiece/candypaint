
import BaseService from './BaseService';
const api = action => `http://localhost.localdomain:4000/api/v1/${action}`;

const UserService = {

    login: (credentials) => {
        return BaseService.post(api('signin'), credentials);
    },

    logOut: () => {
        return BaseService.post(api('logout'));
    },

    isLogged: () => {
        return localStorage.getItem('user') ? true : false;
    },
}
export default UserService;