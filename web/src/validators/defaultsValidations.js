
var defaults = {
    email: { type: "email" },
    first_name: { type: "string", min: 4, max: 30 },
    last_name: { type: "string", min: 4, max: 30 },
    gender: { type: "enum", values: ["male", "female"] },
    password: { type: "string", min: 8, max: 12 },
    password_confirmation: { type: "enum" },
}
export function getDefaultSchema(schema){
    let newSchema = {};
    Object.keys(schema).map( property => {
        newSchema[property]= {
            ...defaults[property],
            ...schema[property]
        }
    })
    return newSchema
} 