import Validator from "fastest-validator"

const validator = new Validator();

export function getValidation(object, schema) {
    return validator.validate(object, schema)
};