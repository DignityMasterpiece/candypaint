import React, { Component } from "react";
import './footer.scss'

class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render() {
    return(
        <footer className="page-footer">
        <div className="footer-copyright">
          <div className="container">
            © 2014 Copyright Text
          <a className="grey-text text-lighten-4 right" href="#!">More Links</a>
          </div>
        </div>
      </footer>
    )
  }
}
export default Footer;