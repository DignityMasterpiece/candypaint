import React, { Component } from "react";
import { injectIntl } from 'react-intl';


class Home extends Component {
  componentDidMount() {
  }
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <b id='a-h'>
        {formatMessage({ id: 'beer' })}    
      </b>
      );
  }
}
export default injectIntl(Home);
