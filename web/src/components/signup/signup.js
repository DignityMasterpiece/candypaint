import React, { Component } from 'react'
import { withRouter } from "react-router-dom";
import { getValidation } from '../../validators/validator';
import { getDefaultSchema } from '../../validators/defaultsValidations';
import './signup.scss';
import { injectIntl } from 'react-intl';
import swal from 'sweetalert';
class SingUp extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userInformation: {
                gender: {
                    male: true,
                    female: false
                }
            }
        }
    }
    componentDidMount() {}

    handleOnChange = (value, prop) => {
        if (prop === 'female' || prop === 'male') {
            value = value === 'on' ? true : false;
            value = prop === 'male' ?
                { male: value, female: !value } :
                { female: value, male: !value };
            prop = 'gender';
        }
        if (typeof value === 'string')
            value = value.trim()
        this.setState({
            userInformation: {
                ...this.state.userInformation,
                [prop]: value
            }
        })
    }

    submit = (event) => {
        event.preventDefault();
        if(this.formValidate()){
            
        }
        return;
    };

    formValidate = () => {
        let { userInformation } = this.state;
        const { formatMessage } = this.props.intl;
        userInformation = {
            ...userInformation,
            gender: userInformation.gender.female === true ? 'female' : 'male'
        };
        let schema = {
            email: null,
            first_name: null,
            last_name: null,
            gender: null,
            password: null,
            password_confirmation: {  values: [userInformation.password] },
        };

        const inputs = ['email', 'gender','first_name','last_name','password','password_confirmation']
        if (inputs.filter( input => !userInformation[input]).length > 0 ) {
            console.log('complete campos');
            swal(
                formatMessage({ id: 'commons.error' }),
                formatMessage({ id: 'commons.error.fields.complete' }),
                'error'
            )
        }
        let validationResult =  getValidation(userInformation,getDefaultSchema(schema));
        console.log(validationResult);
        console.log(Array.isArray(validationResult));
        if(Array.isArray(validationResult)){
            let errorList = '<ul>';
            errorList  += validationResult.map( error => 
                `<li>${formatMessage({id:`commons.error.${error.field}.${error.type}`})}</li>`
            ).join('');
            errorList+='</ul>';
            console.log('epawey',errorList);
            swal(
                formatMessage({ id: 'puto' }),
                {
                    content: (errorList),
                    icon: 'error'
                })
        } else {

        }
    };

    render() {
        const { first_name, last_name, email, password, password_confirmation, gender } = this.state.userInformation;
        const { formatMessage } = this.props.intl;
        return (
            <div id='signup-module'>
                <div className="container">
                    <div className="row">
                        <form className="col s12" id="reg-form">
                            <div className="row">
                                <div className="input-field col s6">
                                    <input
                                        id="first_name"
                                        type="text"
                                        className="validate"
                                        value={first_name || ''}
                                        onChange={e => this.handleOnChange(e.target.value, 'first_name')}
                                        required />
                                    <label htmlFor="first_name">
                                        {formatMessage({ id: 'signup.first_name' })}
                                    </label>
                                </div>
                                <div className="input-field col s6">
                                    <input
                                        id="last_name"
                                        type="text"
                                        className="validate"
                                        value={last_name || ''}
                                        onChange={e => this.handleOnChange(e.target.value, 'last_name')}
                                        required />
                                    <label htmlFor="last_name">
                                        {formatMessage({ id: 'signup.last_name' })}
                                    </label>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='input-field col s12'>
                                    <input
                                        className=''
                                        type='email'
                                        name='email'
                                        id='email'
                                        value={email || ''}
                                        onChange={e => this.handleOnChange(e.target.value, 'email')}
                                    />
                                    <label htmlFor='email'>
                                        {formatMessage({ id: 'signup.email' })}
                                    </label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input
                                        id="password"
                                        type="password"
                                        className="validate"
                                        minLength="6"
                                        value={password || ''}
                                        onChange={e => this.handleOnChange(e.target.value, 'password')}
                                        required />
                                    <label htmlFor="password">
                                        {formatMessage({ id: 'signup.password' })}
                                    </label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input
                                        className=""
                                        type="password"
                                        name="password_confirmation"
                                        value={password_confirmation || ''}
                                        onChange={e => this.handleOnChange(e.target.value, 'password_confirmation')}
                                        id="password_confirm" />
                                    <label htmlFor="password_confirmation" className="active">
                                        {formatMessage({ id: 'signup.password_confirmation' })}
                                    </label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s6">
                                    <p>
                                        <label>
                                            <input
                                                name="gender"
                                                type="radio"
                                                checked={gender.male}
                                                onChange={e => this.handleOnChange(e.target.value, 'male')}
                                            />
                                            <span>
                                                {formatMessage({ id: 'signup.male' })}
                                            </span>
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                            <input
                                                name="gender"
                                                type="radio"
                                                checked={gender.female}
                                                onChange={e => this.handleOnChange(e.target.value, 'female')}
                                            />
                                            <span>
                                                {formatMessage({ id: 'signup.female' })}
                                            </span>
                                        </label>
                                    </p>
                                </div>
                                <div className="input-field col s6">
                                    <button
                                        className="btn btn-large btn-register waves-effect waves-light"
                                        type="submit"
                                        name="action"
                                        onClick={e => this.submit(e)}
                                    >
                                        {formatMessage({ id: 'signup.register' })}
                                        <i className="material-icons right">done</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(injectIntl(SingUp));
