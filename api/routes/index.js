const route = require('express').Router()
const UserController = require('../controllers/userController')
const isAuthentication = require('../middlewares/isAuthentication')

route.post('/signin', [], UserController.signIn)
route.post('/signup', [], UserController.signUp)
route.post('/logout', [isAuthentication], UserController.LogOut)

module.exports = route
