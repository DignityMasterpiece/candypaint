const passport = require('passport')

exports.signIn = (req, res, next) => {
  passport.authenticate('local-signin', (err, user) => {
    if (err) {
      next(err)
    } else {
      console.log(0, user);
      res.send(user)
    }
  })(req, res, next)
};

exports.signUp = (req, res, next) => {
  passport.authenticate('local-signup', (err, user) => {
    if (err) {
      next(err)
    } else {
      res.send(user)
    }
  })(req, res, next)
};

exports.LogOut = (req, res, next) => {
  if(req.session.user) {
  req.session.user = null;
  res.send(true)
  } else {
    return next(new Error('Something went wrong.'));
  }
};
