const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
/**
 * @param {String} fileName - Name of file or files of Environment configuration.
 * @return {Object} - Environment configuration.
 */
const getEnvConfig = (fileName = 'environment') => {
  /**
   * make a merge configuration object a object merge
   * between global and local json file the base object
   * is the global.
   * @return {Object} - Merged object
   */
  const getObjectMerge = () => {
    const globalFile = path.join(__basedir, `${fileName}.json`);
    const localFile = path.join(__basedir, `${fileName}.local.json`);
    let globalConfig, localConfig;
    if (fs.existsSync(globalFile)) {
      try {
        globalConfig = JSON.parse(fs.readFileSync(globalFile, 'utf8'));
        if (fs.existsSync(localFile)) {
          localConfig = JSON.parse(fs.readFileSync(localFile, 'utf8'));
          let env = {};
          Object.keys(globalConfig).map(
            prop => (env[prop] = localConfig[prop] || globalConfig[prop])
          );
          return env;
        } else {
          return globalConfig;
        }
      } catch (error) {
        return { error: error };
      }
    } else {
      let error = 'Environment file not found';
      return { error: error };
    }
  };
  return getObjectMerge();
};
if (getEnvConfig().error) {
  console.log(
    chalk.red('your environment file have a error ->', getEnvConfig().error)
  );
  process.exit();
}

module.exports = getEnvConfig();
//https://www.youtube.com/watch?v=uVltgEcjNww
