const mongoose = require('mongoose')
const { mongodb } = require('./environment')

mongoose.connect(
  `mongodb://${mongodb.host}:${mongodb.port}/${mongodb.db}`,
  { useNewUrlParser: true })
  .then(db => console.log(`${mongodb.db} database connected`))
  .catch(e => console.error(e))
