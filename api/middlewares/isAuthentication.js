/**
 * This is the middleware for the session, it verifies that the user is logged
 */
module.exports = (() => {
  'use strict';
  const isAuthenticated = ((req, res, next) => {
    if (req.session.user) {
      return next();
    }
    return next(new Error('You are not authorized to view this page.'));
  });
  return isAuthenticated;
})();
