module.exports = {

  'notFound': (req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
  },

  'errorDev': (err, req, res, next) => {
    // Shows errors with extra information
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      error: true,
      stack: err.stack,
      object: err
    });
    return;
  },

  'error': (err, req, res, next) => {
    // Shows only error message
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      error: true
    });
    return;
  }
}
