global.__basedir = __dirname;

const express = require('express')
const fs = require('fs')
const morgan = require('morgan')
const passport = require('passport')
const routes = fs.readdirSync(__dirname + '/routes')
const bodyParser = require("body-parser")
const session = require('express-session')
const { deploy } = require('./config/environment')
const errorHandler = require('./middlewares/errorHandler')
const cors = require('cors')
const helmet = require('helmet')

// Initializations
require('./config/database')
require('./passport/local-auth')
const app = express()

//Settings
app.set('port', process.env.port || deploy.port)
let port = app.get('port')
let expiryDate = new Date(Date.now() + 60 * 60 * 1000); // 1 hour

//Middleware
app.use(helmet());
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))
app.use(morgan('dev'))
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))
app.use(passport.initialize())
//app.use(passport.session())
app.use(
  cors({
    credentials: true,
    origin:'http://localhost:3000'
  })
);
/**
 * import all routes file and set each route with  their prefixes
 */

routes.map( (fileNameRoutes) => {
  let prefix =
    fileNameRoutes
      .split('.')[0]
  let baseUrl =
    prefix === 'index' ?
      `/api/v1/` :
      `/api/v1/${prefix}/`
  app.use(baseUrl, require(`./routes/${fileNameRoutes}`))
})

if (app.get('env') === 'development') {
  app.use(errorHandler.errorDev); // development error handler
} else {
  app.use(errorHandler.error); // production error handler
}

//init
app.listen(port, () => {
  console.log(`server start on port ${port}`)
});
module.exports = app;
