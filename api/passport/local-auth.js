const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const User = require('../models/user')
passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id)
  done(null, user)
})

passport.use(
  'local-signup',
  new localStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      const obtainedUser = await User.findOne({ email: email })
      if (obtainedUser) {
        done({ message: 'signup.user.exist' })
      } else {
        let user = new User()
        user.email = email
        user.password = user.encryptPassword(password)
        await user.save()
        done(null, user)
      }
    }
  )
)

passport.use(
  'local-signin',
  new localStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      const obtainedUser = await User.findOne({ email: email })
      if (obtainedUser) {
        if (!obtainedUser.isSamePassword(password)) {
          done({ message: 'signin.user.incorrect_password' })
        } else {
          let userInformation = {
            ...obtainedUser.toObject(),
            password: undefined
          }
          req.session.user = obtainedUser;
          done(null, userInformation)
        }
      } else {
        done({ status: 404, message: 'signin.user.not_found' })
      }
    }
  )
)
